# Building on Windows

Steps:
 * runprebuild.bat
 * Load OpenSim.sln into Visual Studio .NET and build the solution.
 * chdir bin 
 * copy OpenSim.ini.example to OpenSim.ini and other appropriate files in bin/config-include
 * run OpenSim.exe

# Building on Linux

Prereqs:
* Mono > 5.0
* On some Linux distributions you may need to install additional packages.
* See http://opensimulator.org/wiki/Dependencies for more information.

From the distribution type:
 * ./runprebuild.sh
 * type msbuild or xbuild)
 * cd bin 
 * copy OpenSim.ini.example to OpenSim.ini and other appropriate files in bin/config-include
 * windoes: execute opensim.exe or opensim32.exe for small regions
 * linux: run ./opensim.sh
 * msbuild (xbuild) option switches
 *          clean:  msbuild /target:clean
 *          debug: (default) msbuild /property:Configuration=Debug
 *          release: msbuild /property:Configuration=Release


# Building on macOS

Prereqs:
* Mono > 5.0
* Download and uncompress the OpenSim distribution file to a folder (use the Linux distribution file)
* Open terminal to this folder


Steps to complete in terminal:
* ./runprebuild.sh
* msbuild  /p:Configuration=Release
* cd bin
* copy OpenSim.ini.example to OpenSim.ini and other appropriate files in bin/config-include
* mono --server OpenSim.exe
* answer all the questions asked during the startup process. Defaults should be OK for the first run

NOTE: If you want to use MySQL or PostgreSQL additional steps are required to install and configure a
database. You also need to edit connection strings in the configuration files found in the config-include folder.

# References
 
Helpful resources:
* http://opensimulator.org/wiki/Build_Instructions
